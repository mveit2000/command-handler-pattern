﻿using dev.Business.Validators;
using dev.Core.Commands;
using dev.Core.IoC;
using dev.Entities.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace dev.UnitTests
{
    [TestFixture]
    public class HandlerTests
    {
        [Test]
        public void Can_Validate_User()
        {
            var users = new List<User>()
            {
                new User() { LastName = "Smith"},
                new User() { FirstName = "Jack", LastName = "Stith"},
                new User() { LastName = "Snith"},
                new User() { FirstName = "Joe", LastName = "Swith"},
            };
            var mockLocator = new Mock<IServiceLocator>();
            mockLocator.Setup(x => x.Resolve<IValidation>("FirstNameNotNullOrEmpty"))
                       .Returns(new FirstNameNotNullOrEmpty());
            var mockLogger = new Mock<Core.Logger.ILog>();
            var handler = new Handler(mockLogger.Object, mockLocator.Object);

            handler.Add(users[0]);
            var result = handler.Validate<FirstNameNotNullOrEmpty>().Invoke();
            Assert.AreEqual("First name is required.", result.Message);
        }

    }
}
